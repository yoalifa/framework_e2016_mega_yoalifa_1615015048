<?php

use illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

Class BuatTablePengguna extends Migration
{
    /**
    *Run the migrations.
    *
    * @return void
    */

    public function up()
    {
        Schema::create('pengguna', function (Blueprint $table){
            $table->increments('id');
            $table->string('username', 25);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        schema::drop('pengguna');
    }


}

