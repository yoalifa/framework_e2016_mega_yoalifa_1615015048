

@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-default">
<div class="panel-heading">
<strong>Data Buku</strong>
<div class="pull-right">
Tambah Data <a href="{{url('/Buku/tambah')}}"><img src="{{ asset('add.ico') }}" height="20"></img></a>
</div>
<div class="penel-body">
<table class="table">
<tr>
<td> Judul </td>
<td> Kategori </td>
<td> Penerbit </td>
<td> Penulis </td>
<td> Aksi </td>
</tr>
@foreach($datac as $Buku)
<tr>
<td>{{ $Buku->judul }}</td>
<td>{{ $Buku->kategori->deskripsi or 'kosong'}}</td>
<td>{{ $Buku->penerbit }}</td>
<td>{{ $Buku->penulis->first()->nama or 'kosong'}}</td>
<td>
<a href="{{url('Buku/edit/'.$Buku->id)}}"><img src="{{ asset('edit.png') }}" height="20"></img></a>
<a href="{{url('Buku/hapus/'.$Buku->id)}}"><img src="{{ asset('delete.png') }}" height="20"></img></a>
</td>
</tr>
@endforeach
</table>
</div>
</div>
</div>
@endsection