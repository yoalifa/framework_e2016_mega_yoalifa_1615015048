<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penulis extends Model
{
    //
	protected $table = 'penulis';
    protected $fillable=['nama',
    					'notelp',
    					'email',
    					'alamat',
    					];

    public function buku(){
    	return $this->belongsToMany(buku::class);
    }
}

