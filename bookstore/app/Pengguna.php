<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticable;


class Pengguna extends Authenticable
{
    protected $table = 'pengguna';

    protected $fillabe =[ 
   					     'username',
    					  'password',
    					  ];

     protected $hidden = [
        'password',
        'remember_token',
    ];


public function Pembeli(){
		return $this->hasOne(Pengguna::class);
	}

public function Admin(){
        return $this->hasOne(App\Admin);
    }
  
}
