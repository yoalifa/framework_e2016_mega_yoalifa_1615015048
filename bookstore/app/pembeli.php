<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pembeli extends Model
{
    //
    protected $table = 'pembeli';
      protected $fillable=['nama',
    					'no_telp',
    					'email',
    					'alamat',
    					];



    public function Pengguna(){
		return $this->belongsTo(Pengguna::class);
	}

    public function Buku_pembeli(){
        return $this->hasOne(Buku_pembeli::class);
    }


	public function getUsernameAttribute(){
		return $this->pengguna->username;
	}

	public function getPasswordAttribute(){
		return $this->pengguna->password;
	}
}