<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buku extends Model
{
    //
    protected $table = 'buku';


    public function kategori(){
    	return $this->belongsTo('App\Kategori');
    }

    public function penulis(){
    	return $this->belongsToMany(penulis::class)->withPivot('id');
    }
}
